Examples of Security models for MetaEdit+ (www.metacase.com/download). MetaEdit+ enables collaborative modeling, model checking, publishing and most importantly modification and integration of SAM with other languages and generators.

- .mxm file includes just the model data in XML format

It is expected that the repository contains the latest version of the metamodel to import these examples. You may however after importing mxm import some older version of the metamodel. (For import details see https://www.metacase.com/support/55/manuals/meplus/Mp-5_3_2.html) 