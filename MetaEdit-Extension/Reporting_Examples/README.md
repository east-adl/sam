An implementation of SAM for MetaEdit+ (www.metacase.com/download). MetaEdit+ enables collaborative modeling, model checking, publishing and most importantly modification and integration of SAM with other languages and generators.

- .mec file includes the metamodel and example models
    - BBW_SAM.mec as described in “Automotive Cybersecurity Engineering with Modeling Support”, doi.org/10.15439/2024F5017
    - SAMexampleBPMN.mec as described in “Embedded Systems Security Co-Design: Modeling Support for Managers and Developers”
- .mxs file includes library symbols

After importing these two files into MetaEdit+ you may apply SAM. For importing, login to MetaEdit+ and then choose Repository | Import... and start modeling with SAM. (For import details see https://www.metacase.com/support/55/manuals/meplus/Mp-5_3_2.html) 
The main objective for this implementation is being able to integrate SAM with other modeling languages, and extend it as needed. Metamodel extensions can be done with MetaEdit+ Workbench.